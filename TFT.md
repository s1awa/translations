英 Englisch| 德語 Deutsch
:---|:---
7 workouts to sync | 7 Trainingseinheiten zu speichern
NAME | NAME
Home | Start
Training History | Training Historie
Support | Kundendienst
Settings | Einstellungen
7 workouts to sync | 7 Trainingseinheiten zu speichern
OR | ODER
Email | Email
Password | Passwort
Forgot Password? | Passwort vergessen?
LOG IN | EINLOGGEN
Create an Account | Konto erstellen
Scan the QR Code | Scannen Sie den QR-Code
How to log in by QR code? | Wie logge ich mich mit QR-Code ein?
Email | Email
Password | Passwort
Confirm Password | Passwort bestätigen
LOG IN | EINLOGGEN
Step 1 | Schritt 1
 | 
Use iC+Training scanner to log in. iConsole+ Training\Menu\ Membership\QR code scanner" | Melden Sie sich mit dem iC + Training-Scanner an. iConsole + Training \ Menü \ Mitgliedschaft \ QR-Code-Scanner
Step 2 | Schritt 2
 | 
Tap log in for permission. | Tippe auf Anmelden um Erlaubnis zu geben.
My Training | Mein Training
Get Started | Loslegen
Let's go training | Lass uns trainieren gehen
Workout sessions this week: | Trainingseinheiten diese Woche:
Total Distance | Gesamtentfernung
Total Time | Gesamtzeit
Total Calories | Gesamtkalorien
AVG. Workout Distance | Ø Trainingsabstand
AVG. Calories Burned | Ø Kalorien verbrannt
km | km
cal | kal
My Training | Mein Training
Get Started | Loslegen
Quick Start | Schnellstart
Run and count laps | Renne und zähle Runden
Interval | Intervall
Train less, workout big | Trainiere weniger, trainiere kräftig
Map your route | Eigene Route
Plan a route and run anywhere | Plane eine Route und laufe überall
Interval - 5 cycles | Intervall - 5 Zyklen
SET YOUR TRAINING CYCLES, TIME, LEVEL AND TARGET RPM. | SETZEN SIE IHRE TRAININGSZYKLEN, ZEIT, STUFE UND SOLL RPM.
total time: | Gesamtzeit:
cycle | Zyklus
High Intensity | Hohe Intensität
Rest Time | Ruhezeit
Time | Zeit
Level | Stufe
Target rpm | SOLL RPM
START | START
Interval - 5 cycles | Intervall - 5 Zyklen
high intensity 1/5 | hohe Intensität 1/5
total time: | Gesamtzeit
Level | Stufe
Level Down | Stufe runter
Level Up | Stufe hoch
Distance | Entfernung
Speed | Geschwindigkeit
RPM | RPM
Heart Rate | Puls
HR Zone | Puls-Zone
BAI TM | BAI TM
Calories | Kalorien
Watt | Watt
km | km
bpm | bpm
cal | kal
RESUME | FORTSETZEN
PAUSE | PAUSE
STOP | STOP
Map Your Route | Eigene Route
Where to go? | Wo hin?
Map Your Route | Eigene Route
Slide to Start | Wischen um zu beginnen
km | km
GO | LOS
Map Your Route | Eigene Route
Time | Zeit
Distance | Entfernung
Speed | Geschwindigkeit
Calories | Kalorien
HR Zone | Puls-Zone
RPM | RPM
Heart Rate | Puls
Watt | Watt
BAI TM | BAI TM
km | km
km/hr | km/h
cal | kal
km/h | km/h
rpm | rpm
(m) | (m) 
(km) | (km)
LEVEL | Stufe
LEVEL up | Stufe hoch
LEVEL down | Stufe runter
Map Your Route | Eigene Route
Time | Zeit
Distance | Entfernung
Speed | Geschwindigkeit
Calories | Kalorien
HR Zone | Puls-Zone
Avg Pace | Ø Tempo
Heart Rate | Puls
BAI TM | BAI TM
km | km
km/hr | km/h
cal | kal
min/km | min/km
rpm | rpm
(m) | (m)
(km) | (km) 
Incline | Neigung
Speed | Geschwindigkeit
Quick Start, Set up your targets. | Schnellstart, Richten Sie Ihre Ziele ein.
SET UP YOUR TARGET TIME, DISTANCE OR FAT TO BURN. | SETZE DEINE ZIELE: ZEIT, ENTFERNUNG ODER FETTVERBRENNUNG.
Distance | Entfernung
1.4 km | 1,4 km
fat to burn | Fett zu verbrennen
250 cal | 250 Kal
START | START
Quick Start | SCHNELLSTART
LAP 5 | RUNDE 5
level | Stufe
km | km
m | m
Level Down | Stufe runter
Level Up | Stufe hoch
Distance | Entfernung
Speed | Geschwindigkeit
RPM | RPM
Heart Rate | Puls
HR Zone | Puls-Zone
BAI TM | BAI TM
Calories | Kalorien
Watt | Watt
PAUSE | PAUSE
RESUME | FORTSETZEN
STOP | STOP
Training History | Training Historie
Workouts: | Trainingseinheiten:
Calories: | Kalorien:
Duration: | Dauer:
Bike/Elliptical | Fahrrad / Elliptisch
Interval | Intervall
km | km
AVG. HR: | Ø Puls
bpm | bpm
cal | kal
Easy | Einfach
AVG Speed: | Ø Geschwindigkeit:
km/h | km/h
Result | Ergebnis
Machine: | Maschine:
Training Mode: | Trainingsmodus:
BAI TM: | BAI TM:
Duration: | Dauer:
Distance: | Entfernung:
Calories: | Kalorien:
Cycles: | Zyklen:
High Intensity: | Hohe Intensität:
Rest Time: | Ruhezeit:
MAX Altitude | max. Höhe
AVG. HR: | Ø Puls
AVG. Pace: | Ø Tempo:
AVG Speed: | Ø Geschwindigkeit:
I feel... | Es war...
Somewhat easy | ziemlich einfach
Connect | Verbinden
leave a message... | hinterlasse eine Nachricht...
Result | Ergebnis
Machine: | Maschine:
Training Mode: | Trainingsmodus:
BAI TM: | BAI TM:
Duration: | Dauer:
Distance: | Entfernung:
Calories: | Kalorien:
Cycles: | Zyklen:
High Intensity: | Hohe Intensität:
Rest Time: | Ruhezeit:
MAX Altitude | max. Höhe
AVG. HR: | Ø Puls
AVG. Pace: | Ø Tempo:
AVG Speed: | Ø Geschwindigkeit:
I feel... | Es war...
Somewhat easy | ziemlich einfach
Connect | Verbinden
leave a message... | hinterlasse eine Nachricht...
Result | Ergebnis
Machine: | Maschine:
Training Mode: | Trainingsmodus:
BAI TM: | BAI TM:
Duration: | Dauer:
Distance: | Entfernung:
Calories: | Kalorien:
Lap: | Runde:
High Intensity: | Hohe Intensität:
Rest Time: | Ruhezeit:
AVG. HR: | Ø Puls:
AVG. RPM: | Ø RPM:
AVG. Watt: | Ø Watt:
AVG. Pace: | Ø Tempo:
AVG Speed: | Ø Geschwindigkeit:
I feel... | Es war...
Somewhat easy | ziemlich einfach
Comments | Bemerkungen
leave a message... | hinterlasse eine Nachricht...
Settings | Einstellungen
Display Units | Einheiten
Heart Rate | Puls
Version | Version
Build | Build
Membership | Mitgliedschaft
Date of birth | Geburtsdatum
Height | Höhe
Weight | Gewicht
Gender | Geschlecht
Region | Region
Login type | Anmeldungsart
Summary | Zusammenfassung
Machine: | Maschine:
Training Mode: | Trainingsmodus:
BAI TM: | BAI TM:
Duration: | Dauer:
Distance: | Entfernung:
Calories: | Kalorien:
Lap: | Runde:
High Intensity: | Hohe Intensität:
Rest Time: | Ruhezeit:
AVG. HR: | Ø Puls:
AVG. RPM: | Ø RPM:
AVG. Watt: | Ø Watt:
AVG. Pace: | Ø Tempo:
AVG Speed: | Ø Geschwindigkeit: 
I feel... | Es war...
Somewhat easy | ziemlich einfach
Comments | Bemerkungen
leave a message... | hinterlasse eine Nachricht...
Searching HR strap . . . | Suche nach Pulsband
PolarHR | PolarHR
MIO ALFA | MIO ALFA 
WAHOO HR | WAHOO HR
connected | verbunden
DONE | ERLEDIGT
Distance | Entfernung
Calories | Kalorien
Heart Rate | Puls
Time | Zeit
Watt | Watt
HR Zone | Puls-Zone
RPM/SPM | RPM/SPM
km | km
cal | kal
rpm | rpm
km/h | km/h
Speed | Geschwindigkeit
Incline | Neigung
 | 
Quick Start | SCHNELLSTART
COMMUNITY CENTER | COMMUNITY
MANUAL | HANDBUCH
PROGRAM | PROGRAMM
WATT CONSTANT | WATT KONSTANT
TARGET H.RATE | ZIEL PULS
EXIT | VERLASSEN
Youtube | YouTube
Netflix | Netflix
Spotify | Spotify
Chrome | Chrome
i Route | i Route
kinomap | kinomap
HOME | ZURÜCK
QUICK START | SCHNELL START
SPEED | GESCHWINDIGKEIT
LOAD | BELASTUNG
DISTANCE | ENTFERNUNG
TIME | ZEIT
RPM | RPM
WATT | WATT
CALORIES | KALORIEN
PULSE | PULS
km | km
KCL | KCL
EXIT | VERLASSEN
START | START
MANUAL | HANDBUCH
User | Benutzer
AGE | ALTER
HEIGHT | HÖHE
WEIGHT | GEWICHT
Male | Männlich
Female | Weiblich
TIME | ZEIT
DISTANCE | ENTFERNUNG
CALORIES | KALORIEN
PULSE | PULS
cm | cm
kg | kg
km | km
cal | kal
EXIT | VERLASSEN
START | START
PROGRAM | PROGRAM
Male | Männlich
Famale | Weiblich
User | Benutzer
TIME | ZEIT
AGE | ALTER
HEIGHT | HÖHE
WEIGHT | GEWICHT
cm | cm
kg | kg
PAUSE | PAUSE
HOME | ZURÜCK
UP | HOCH
DOWN | RUNTER
RECOVERY | WIEDERHERSTELLUNG
LOAD | BELASTUNG
PULSE | PULS
WATT | WATT
TIME | ZEIT
SPEED | GESCHWINDIGKEIT
DISTANCE | ENTFERNUNG
RPM | RPM
CALORIES | KALORIEN
km | km
KCL | KCL
HOME | ZURÜCK
CONTINUE | FORTSETZEN
PAUSE | PAUSE
Please press CONTINUE to go on or press HOME to quit the exercise | Bitte drücken Sie WEITER, um fortzufahren, oder drücken Sie ZURÜCK, um die Übung zu beenden
SPEED | GESCHWINDIGKEIT
DISTANCE | ENTFERNUNG
RPM | RPM
CALORIES | KALORIEN
km | km
KCL | KCL
EXIT | VERLASSEN
START | START
WATT CONSTANT | WATT KONSTANT
WATT | WATT
Male | Männlich
Famale | Weiblich
User | Benutzer
TIME | ZEIT
AGE | ALTER
HEIGHT | HÖHE
WEIGHT | GEWICHT
cm | cm
kg | kg
PAUSE | PAUSE
HOME | ZURÜCK
RECOVERY | WIEDERHERSTELLUNG
TARGET WATT CONSTANT DATA | ZIEL WATT KONSTANTE DATEN
REAL WATT CONSTANT DATA | REALE WATT KONSTANTE DATEN
LOAD CONTROL DATA : LOAD | LOAD CONTROL DATEN: LADEN
SPEED | GESCHWINDIGKEIT
RPM | RPM
TIME | ZEIT
DISTANCE | ENTFERNUNG
CALORIES | KALORIEN
PULSE | PULS
km | km
KCL | KCL
Reminder : Please slow down your speed. | Erinnerung: Bitte verlangsamen Sie Ihre Geschwindigkeit.
EXIT | VERLASSEN
START | START
TARGET H.RATE | ZIEL PULS
HEART RATE | PULS
Male | Männlich
Famale | Weiblich
User | Benutzer
TIME | ZEIT
AGE | ALTER
HEIGHT | HÖHE
WEIGHT | GEWICHT
cm | cm
kg | kg
PAUSE | PAUSE
HOME | ZURÜCK
RECOVERY | WIEDERHERSTELLUNG
TARGET HEART RATE DATA : BPM | ZIEL-PULS-DATEN: BPM
REAL HEART RATE DATA : BPM | ECHTE PULS DATEN: BPM
LOAD CONTROL DATA : LOAD | LOAD CONTROL DATEN: LADEN
SPEED | GESCHWINDIGKEIT
RPM | RPM
TIME | ZEIT
DISTANCE | ENTFERNUNG
CALORIES | KALORIEN
WATT | WATT
km | km
KCL | KCL
Reminder : when you see the message if means the PULSE INPUT is not read , you have to hold the grips tightly. | Zur Erinnerung: Wenn Sie die Meldung sehen, dass der PULS nicht empfangen wird, müssen Sie die Griffe festhalten.
RECOVERY | WIEDERHERSTELLUNG
To test heart rate recovery status. | Um den Puls-Wiederherstellungsstatus zu testen.
sec | sec
TOUCH THE SCREEN TO LEAVE THE PROGRAM | BERÜHREN SIE DEN BILDSCHIRM, UM DAS PROGRAMM ZU VERLASSEN
Please stop the exercise and keep holding handles | Bitte halte die Übung an und halte die Griffe fest
RECOVERY | WIEDERHERSTELLUNG
To test hear rate recovery status. | Um den Puls-Wiederherstellungsstatus zu testen.
sec | sec
TOUCH THE SCREEN TO LEAVE THE PROGRAM | BERÜHREN SIE DEN BILDSCHIRM, UM DAS PROGRAMM ZU VERLASSEN
Your Recovery Rate Number is F6 | Ihre Wiederherstellungsrate ist F6
Condition WORST | Zustand SCHLECHT